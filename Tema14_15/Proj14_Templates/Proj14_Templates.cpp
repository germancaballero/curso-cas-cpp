// Proj14_Templates.cpp : 

#include <iostream>
#include <string>
#include "Pareja.h"
#include "Trio.h"

using namespace std;

int GetMax(int a, int b) {
    return (a > b) ? a : b;
}
float GetMax(float a, float b) {
    return (a > b) ? a : b;
}
template <class Tipo>   // Lo normal es llamarlo T
Tipo GetMaxT(Tipo a, Tipo b) {
    Tipo resultado;
    resultado = (a > b) ? a : b;
    return resultado;
}
template <class T, class U>   // Lo normal es llamarlo T
T GetMin(T a, U b) {
    return a < b ? a : b;
}


int main()
{
    int a = 5, b = 10;
    cout << "Maximo de 5 y 10: " << GetMax(a, b) << " \n";
    float af = 5.1f, bf = 10.1f;
    cout << "Maximo de 51.1 y 10.1: " << GetMax(af, bf) << " \n";

    cout << "CON TEMPLATES: \n";
    cout << "Maximo de 5 y 10: " << GetMaxT<int>(a, b) << " \n";
    cout << "Maximo de 5.1 y 10.1: " << GetMaxT<float>(af, bf) << " \n";
    double ad = 5.11111111111111111111111, 
        bd = 10.111111111111111111111;
    double maxd = GetMaxT<double>(ad, bd);
    cout << "Maximo de 5.1111... y 10.111...: " << maxd << " \n";

    cout << "Maximo de 5.111.... y 10: " << GetMin<double, int>(ad, b) << " \n";
    Pareja<int> unpar(115, 36);
    cout << "Maximo de pareja 115: " << unpar.GetMax() << " \n";
    // Pareja<string> par_textos(string("ABC"), string("CDE"));
    Pareja<string>* par_textos;
    par_textos  = new Pareja<string>(string("ABC"), string("CDE"));
    cout << "Maximo de pareja ABC: " << par_textos->GetMax() << " \n";

    Trio<int> lostres(30, 50, 90);
    cout << "Maximo de trio 90: " << lostres.GetMax() << " \n";
    cout << "Minimo de trio 30: " << lostres.GetMin() << " \n";

    Trio<string> tres_string("ABC", "CDE", "EFG");
    cout << "Maximo de trio EFG: " << tres_string.GetMax() << " \n";
    cout << "Minimo de trio ABC: " << tres_string.GetMin() << " \n";
}
// Crea una clase template, que contenga un Trio (3 elementos del mismo tipo) y pueda devolver el máximo y el mínimo de los 3.
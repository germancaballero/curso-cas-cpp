#pragma once
template <class T>
class Trio
{
    T valores[3];
public:
    Trio(T primer, T segun, T tercer) {
        valores[0] = primer;
        valores[1] = segun;
        valores[2] = tercer;
    }
    T GetMin() {
        return (valores[0] < valores[1] && valores[0] < valores[2]) ? 
              valores[0] 
            : (valores[1] < valores[2] ?
                    valores[1] 
                  : valores[2]);
    }
    T GetMax() {
        if (valores[0] > valores[1] && valores[0] > valores[2]) {
            return valores[0];
        } 
        else if (valores[1] > valores[2])
            return valores[1];
        else
            return valores[2];
    }
};


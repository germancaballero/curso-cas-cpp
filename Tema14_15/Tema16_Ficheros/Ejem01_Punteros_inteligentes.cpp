//https://www.genbeta.com/desarrollo/repaso-a-c-11-el-principio-raii-y-los-cuatro-miembros-implicitos
// https://docs.microsoft.com/es-es/cpp/cpp/smart-pointers-modern-cpp?view=msvc-160

// 1 - Leer un fichero de codigo fuente con fich.getline()
// 2 - Crear una cadena (string) con tooodo el fichero
// 3 - Mostrar la longitud (tama�o en) del fichero
// 4 - Contar cuantos puntos y coma tiene (string.at() � [])

// 5 - Controlar mediante try/catch que existe el fichero
// 6 - Eliminar los comentarios, las lineas que empiezan por // acordaros de .erase()
// 7 - Guardar el mismo fichero fuente sin comentarios
#include <iostream> 
#include <fstream>  // Librer�a para funciones de ficheros

using namespace std;

string* leer_fichero_ptr ( ifstream& fich )
{
   char linea_codigo [ 1024 ];

   string* todo_fich = new string ( "" );
   while ( !fich.eof ( ) )
   {
      fich.getline ( linea_codigo , 1024 );
      todo_fich->append ( linea_codigo ).append ( !fich.eof ( ) ? "\r\n" : "" );
   }
   cout << "Tamanho: " << todo_fich->length ( ) << endl;
   return todo_fich;
}

int main ( )
{
   ifstream fich_codigo;
   string* ptr_text = NULL;
   try
   {
      fich_codigo.open ( "Ejercicio_Ficheros.cpp" );

      if ( fich_codigo.fail ( ) )
      {
         throw exception ( "Fallo No se ha podido abrir" );
      } else
      {
         ptr_text = leer_fichero_ptr ( fich_codigo );
         unique_ptr<string> texto_fich( ptr_text );
         cout << *texto_fich << endl;
      }
   } catch ( exception& ex )
   {
      cout << ex.what ( ) << "\n";
   }
   //  delete ptr_text; // Ya no tenemos que liberar la memoria porque se ha hecho de manera autom�tica con unique_ptr
}
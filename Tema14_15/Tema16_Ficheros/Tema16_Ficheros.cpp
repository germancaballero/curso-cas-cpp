// Tema16_Ficheros.cpp 

#include <iostream>
#include <fstream>

using namespace std;

void main_ejemplo()
{
    cout << "mi_fichero.txt!\n";
    // Output File Stream: Flujo de fichero de salida
    std::ofstream fichero ( "mi_fichero.txt" );
    fichero << "Hola, bienvenidos!" << endl;
    fichero << "Ottra linea del fichero" << endl;
    fichero.close ( );

    ifstream fich_entrada ( "mi_fichero.txt" );

    char linea1 [ 58 ] , linea2 [ 58 ];
    fich_entrada.getline ( linea1 , 58 );
    fich_entrada.getline ( linea2 , 58 );

    cout << "LEIDO: linea 1 = '" << linea1 << "',"
       << "\nlinea 2 = '" << linea2 << "'\n";

    char linea_codigo [ 1024 ];

    ifstream fich_codigo ( "Tema16_Ficheros.cpp" );

    while ( !fich_codigo.eof ( ) )
    {
       fich_codigo >> linea_codigo;
       cout << linea_codigo << endl;
    }
    fich_codigo.close ( );

    unsigned char caracter;
    fich_codigo.open ( "Tema16_Ficheros.cpp" );
    while ( !fich_codigo.eof ( ) )
    {
       fich_codigo >> caracter;
       if ( caracter <= 32 )
          cout << " [ " << ( int ) caracter << " ] " << endl;
       cout << caracter;
    }
}

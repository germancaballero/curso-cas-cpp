
// 1 - Leer un fichero de codigo fuente con fich.getline()
// 2 - Crear una cadena (string) con tooodo el fichero
// 3 - Mostrar la longitud (tama�o en) del fichero
// 4 - Contar cuantos puntos y coma tiene (string.at() � [])

// 5 - Controlar mediante try/catch que existe el fichero
// 6 - Eliminar los comentarios, las lineas que empiezan por // acordaros de .erase()
// 7 - Guardar el mismo fichero fuente sin comentarios
#include <iostream> 
#include <fstream>  // Librer�a para funciones de ficheros

using namespace std;

string* leer_fichero ( ifstream& fich )
{
   char linea_codigo [ 1024 ];

   string* todo_fich = new string ( "" );
   while ( !fich.eof ( ) )
   {
      fich.getline ( linea_codigo , 1024 );
      todo_fich->append ( linea_codigo ).append ( !fich.eof ( ) ? "\r\n" : "" );
   }
   cout << "Tamanho: " << todo_fich->length ( ) << endl;
   return todo_fich;
}

int buscar_puntos_comas ( string* todo_fich )
{
   int num_puntos_comas = 0;
   for ( int i = 0; i < todo_fich->length ( ); i++ )
   {
      if ( todo_fich->at ( i ) == ';' )
      {
         num_puntos_comas++;
      }
   }
   cout << "Numero de puntos y comas: " << num_puntos_comas << endl;
   return num_puntos_comas;
}
void main_ejercicio ( )
{
   string* texto_fich = (string*) 45;
   ifstream fich_codigo;

   try
   {
      fich_codigo.open ( "Ejercicio_Ficheros.cpp" );

      if ( fich_codigo.fail ( ) )
      {
         throw exception ( "Fallo No se ha podido abrir" );
      } else
      {

         texto_fich = leer_fichero ( fich_codigo );
         cout << *texto_fich << endl;
         buscar_puntos_comas ( texto_fich );
      }
   } catch ( exception& ex )
   {
      cout << ex.what ( ) << "\n";
   }
   delete texto_fich;
}
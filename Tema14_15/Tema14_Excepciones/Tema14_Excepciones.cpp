#include <iostream>

#include "ExcepcionTest.h"

using namespace std;

void fun_provocar_stackorverflow ( int z )
{
   int x , y;
   if ( z % 100 == 0 )
      cout << "Entrando en fun() con z = " << z << "\n";

   if ( z < 400 )
      fun_provocar_stackorverflow ( ++z );
   else
      throw new ExcepcionTest ( "Probable stack overflow" , 5454 );
      
   if ( z % 100 == 0 )
      cout << "Saliendo de la fun(), volviendo " << z << "\n";
}


void funcion_sin_excepciones ( ) noexcept
{
   // Con esto evitamos la comprobación de excepciones, incrementa muy poco el rendimiento. 
   // Si ocurre una excepcion, el programa termina sin indicar qué ha pasado
   cout << "Sabemos que no provocaremos exepcion\n";
}


int main()
{
    std::cout << "Provocar stack overflow!\n";
    try
    {
       fun_provocar_stackorverflow ( 0 );
    } catch ( invalid_argument& ex )
    {
       // No se llega a llamar porque no se lanza
    } catch ( ExcepcionTest& ex )
    {
       cout << "Excepcion:" << ex.what ( ) << ", cod err: " << ex.getCodError ( ) << endl;
    }
    catch ( exception& ex )
    {
       cout << "Excepcion:" << ex.what ( ) << endl;
    }
    catch ( ... )
    {
       cout << "Excepcion!!!" << endl;
    }
    funcion_sin_excepciones ( );
}

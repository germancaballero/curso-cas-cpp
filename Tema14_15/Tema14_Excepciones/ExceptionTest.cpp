#include "ExcepcionTest.h"

ExcepcionTest::ExcepcionTest ( const char* mensaje , int ce )
	: exception ( mensaje )
{
	this->codigo_error = ce;
}

int ExcepcionTest::getCodError ( )
{
	return this->codigo_error;
}
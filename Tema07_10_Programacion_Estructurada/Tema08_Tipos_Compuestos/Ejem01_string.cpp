#include "Header.h"

void ejem_string ( )
{
   // Internamente es un array din�mico de caracteres, con la caracter�stica de que puede reservar directamente de un const char*
   string texto = "Hola que pasa!";
   cout << texto << endl;
   cout << "Tama�o: " << texto.size() << endl;

   cout << "append: " << texto.append ( " Poca cosa" ) << endl;
   cout << "at 5:   " << texto.at ( 5 ) << endl;
   cout << "substr 5, 6:   " << texto.substr ( 5, 6 ) << endl;

}
#include "Header.h"
/* 
  Asterisco: * -> adem�s multipicaci�n, 
                  declaraci�n puntero (int* var_ptr, int *var_ptr) y tambi�n como 
                  operador unario de indirecci�n (muestra el contenido de un puntero: (*var_ptr) += 3)
  Ampersand: & -> adem�s del & bit a bit, y del && and booleano
                  operador unario de direcci�n de memoria de una variable */

void mostrar_array_struct ( usuario [ ] , int t );

void ejem_array_struct ( )
{
   usuario usuarios [ 3 ]; // Se reservan directamente los (20 + 4 + 4) * 3 = 84; 
   strcpy_s ( usuarios [ 0 ].nombre , "Fulanito" );
   usuarios [ 0 ].edad = 30;
   usuarios [ 0 ].altura = 1.7;

   strcpy_s ( usuarios [ 1 ].nombre , "Menganito" );
   usuarios [ 1 ].edad = 24;
   usuarios [ 1 ].altura = 1.6;
   mostrar_array_struct ( usuarios , 2 );
   mostrar_array_struct ( usuarios , 2 );
   
}

void mostrar_struct ( usuario *usuario)
{
   cout << "Nombre   : " << usuario->nombre << endl;
   cout << "- Edad   : " << usuario->edad << endl;
   cout << "- Altura : " << usuario->altura << endl;
}
void mostrar_array_struct ( usuario usuarios [ ] , int tam )
{
   // usuarios [ 0 ].edad++;
   ( *( usuarios + 0 ) ).edad++;
   for ( int i = 0; i < tam; i++ )
   {
      cout << "Usuario   " << i << ": " << endl;
      mostrar_struct ( & usuarios [ i ] );
   }
}

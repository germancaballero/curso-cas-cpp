#pragma once

#include <iostream>

using namespace std;

void ejemplo_struct ( ); 
void ejem_array_int ( );
void ejem_array_char ( );
void ejem_array_struct ( );
void ejercicio_array_struc ( );
void ejercicio_array_struc_mem ( );

struct usuario
{
   char nombre [ 20 ];
   int edad;
   float altura;
};
void ejem_string ( );
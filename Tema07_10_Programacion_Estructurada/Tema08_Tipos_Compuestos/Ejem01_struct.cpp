#include "Header.h"

struct punto_3d
{
   float x, y , z;
} punto_a, punto_b;

struct punto_2d
{
   float x;
   float y;
} punto_h;

punto_3d punto_c;

// Paso por valor: (Se copia todo el contido del argumento y se pasa como par�metro, otra direcci�n de memoria)
void mostrar_punto_3d ( punto_3d p3d )
{
   cout << "Punto: (" << p3d.x << ", " << p3d.y << ", " << p3d.z << ") \n";
   // Cambiamos el valor:
   p3d.x = 200;
   cout << "Ahora p3d.x = " << p3d.x << endl;
   // cout << "Posicion memoria p3d = " << (void*) ( int ) &p3d << endl;
   cout << "Posicion memoria p3d = " <<  &p3d << endl;
}
void mostrar_punto_3d ( punto_3d* ptr_p3d, int *ptr_entero)
{
   cout << "Punto desde PUNTERO: (" << ptr_p3d->x << ", " << ptr_p3d->y << ", " << ptr_p3d->z << ") \n";
   // Cambiamos el valor:
   ptr_p3d->x = 200;
   cout << "Ahora ptr_p3d->x = " << ptr_p3d->x << endl;
   cout << "Contenido        ptr_entero = " << ( *ptr_entero ) << endl;  // Equivalente a ptr_p3d->x 
   cout << "Contenido        ptr_p3d.x = " << (*ptr_p3d).x << endl;  // Equivalente a ptr_p3d->x 

   cout << "Contenido        ptr_p3d = " << ptr_p3d << endl;
   cout << "Posicion memoria ptr_p3d = " << &ptr_p3d << endl;
   int** ptr_ptr_entero = & ptr_entero;
   cout << "Posicion memoria ptr_entero = " << &ptr_entero << endl;
}

void ejemplo_struct ( )
{
   punto_3d punto_d;
   punto_a.x = 10;
   punto_a.y = 0;
   punto_a.z = 0;

   punto_c.x = 0 , punto_c.y = 7 , punto_c.z = 0;

   punto_h.x = 3 , punto_h.y = 3;

   // cout << "Posicion memoria punto_a = " << ( void* ) ( int ) &punto_a << endl;
   cout << "Posicion memoria punto_a = " << ( void* ) ( size_t ) &punto_a << endl;
   mostrar_punto_3d ( punto_a );
   mostrar_punto_3d ( punto_c );
   cout << "punto_c.x = " << punto_c.x << endl;
   //   punto_h = ( punto_2d) punto_a;
   int num = 66;
   mostrar_punto_3d ( & punto_a, &num );
   mostrar_punto_3d ( & punto_c , &num );
   cout << "punto_c.x = " << punto_c.x << endl;
}

/*
   Realizar el mismo ejemplo Ejem02_struct.cpp pero como un array de punteros a estructura (en vez de un array a estructura).
*/
#include "Header.h"

void ej_mostrar_array_struct ( usuario** , int t );
void ej_mostrar_struct ( usuario* usuario );

void ejercicio_array_struc ( )
{
   int i;
   int *ptr_i;
   ptr_i = &i;
   usuario* usuarios [ 3 ]; // Se reservan directamente los (4 � 8) * 3 = 12 � 24 bytes; 
   usuario u1, u2;
   strcpy_s ( u1.nombre , "Fulanito" );
   u1.edad = 30;
   u1.altura = 1.7;

   strcpy_s ( u2.nombre , "Menganito" );
   u2.edad = 24;
   u2.altura = 1.6;

   usuarios [ 0 ] = &u1;
   usuarios [ 1 ] = &u2;

   ej_mostrar_array_struct ( usuarios , 2 );
   ej_mostrar_array_struct ( usuarios , 2 );
}

void ejercicio_array_struc_mem ( )
{
   // FORMA 1: 
   // usuario* usuarios [ 3 ]; // Se reservan directamente los (4 � 8) * 3 = 12 � 24 bytes; 
   // FORMA 2, todo din�mico:
   usuario** usuarios = ( usuario** ) malloc ( sizeof ( usuario *) * 3 );

   usuarios [ 0 ] = ( usuario* ) malloc ( sizeof ( usuario ) );
   usuarios [ 1 ] = ( usuario* ) malloc ( sizeof ( usuario ) );
   usuarios [ 2 ] = ( usuario* ) malloc ( sizeof ( usuario ) );
   strcpy_s ( usuarios [ 0 ]->nombre , "Fulanito" );
   (*usuarios [ 0 ]).edad = 30;
   usuarios [ 0 ]->altura = 1.7;

   strcpy_s ( usuarios [ 1 ]->nombre , "Menganito" );
   usuarios [ 1 ]->edad = 24;
   usuarios [ 1 ]->altura = 1.6;

   ej_mostrar_array_struct ( usuarios , 2 );
   ej_mostrar_array_struct ( usuarios , 2 );

   cout << "Vamos a borrar usu 0:  " << usuarios [ 0 ] << " � " << ( *usuarios ) << endl;
   free ( usuarios [ 0 ] );   // Tambi�n podr�amos usar     free (* usuarios );  �  free (* (usuarios+0) );
   free ( usuarios [ 1 ] );   // Tambi�n podr�amos usar     free (* ( usuarios + 1) );
   free ( usuarios [ 2 ] );   // Tambi�n podr�amos usar     free (* ( usuarios + 2) );
   
   // free (* usuarios );  este es el puntero a la primera estructura: igual que usuarios[0]
   free ( usuarios );
}
void ej_mostrar_struct ( usuario* usuario )
{
   cout << "Nombre   : " << usuario->nombre << endl;
   cout << "- Edad   : " << usuario->edad << endl;
   cout << "- Altura : " << usuario->altura << endl;
}
// void mostrar_array_struct ( usuario* usuarios [ ] , int tam )
void ej_mostrar_array_struct ( usuario** usuarios , int tam )
{
   // usuarios [ 0 ].edad++;
   ( *( usuarios + 0 ) ) -> edad++;
   for ( int i = 0; i < tam; i++ )
   {
      cout << "Usuario   " << i << ": " << endl;
      ej_mostrar_struct ( usuarios [ i ] );
   }
}

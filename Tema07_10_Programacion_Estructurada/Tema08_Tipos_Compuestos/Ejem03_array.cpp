#include "Header.h"

#define TAM_ARRAY 4

float calcula_media ( int notas_alumnos [ ] , int tamano );
float calcula_media_ptr ( int* notas_alumnos , int tamano );
void mostrar_texto ( char* texto );
void mostrar_texto_2 ( const char* texto );

void ejem_array_char ( )
{
   char texto [ 30 ];
   strcpy_s ( texto , "Hola que hay" );
   mostrar_texto ( texto );
   mostrar_texto_2 ( "Hola que hay!!!!" );
}
void mostrar_texto ( char* texto )
{
   cout << texto << endl;
}
void mostrar_texto_2 (const char* texto )
{
   cout << texto << endl;
}

void ejem_array_int ( )
{
   // Un array es un tipo de dato que contiene un conjunto de elementos del mismo tipo de tama�o est�tico.
   int notas_alumnos [ TAM_ARRAY ]; // 19 es el tama�o
   notas_alumnos [ 0 ] = 7;   // El primer alumno, los �ndices empiezan por cero, contiene 7
   notas_alumnos [ 1 ] = 9;   // 2do
   notas_alumnos [ 2 ] = 2;   // 3do
   notas_alumnos [ 3 ] = 5;
   cout << "El 3er alumno tiene: " << notas_alumnos [ 2 ] << endl;
   int i = 0;
   // float media = calcula_media ( notas_alumnos , TAM_ARRAY );
   // float media = calcula_media_ptr ( & notas_alumnos[0] , TAM_ARRAY );
   // La siguiente l�nea es equivalente a la anterior: UN ARRAY ES UN PUNTERO A SU PRIMER ELEMENTO
   float media = calcula_media_ptr ( notas_alumnos , TAM_ARRAY );
   
   for ( ; i < TAM_ARRAY; i++ )
   {
      cout << "El alumno " << i << " tiene: " << notas_alumnos [ i ] << endl;
   }
   // Oye, que la i ahora es otra cosa... bueno, ya no
   cout << "Total de alumnos: " << TAM_ARRAY << ", media = " << media << endl;
}

float calcula_media ( int notas_alumnos [], int tamano )
{
   // Aqu� trabajamos con el array como si fuera un puntero, por que lo es
   cout << "notas_alumnos = " << notas_alumnos << endl;
   int sumatorio = 0;
   float media = 0;
   *( notas_alumnos + 2 ) += 3;   // Usa aritm�tica de puntero. al sumar 2 suma en realidad 2 * 4
   for ( int i = 0; i < TAM_ARRAY; i++ )
   {
      sumatorio += *( notas_alumnos + i );
   }
   media = ( float ) sumatorio / TAM_ARRAY;
   tamano = 200;  // NO afecta fuera por ser tipo primitivo, no un puntero
   return media;
}

float calcula_media_ptr ( int* notas_alumnos, int tamano )
{
   // Aqu� trabajamos con el puntero como si fuera un array, por que lo es
   cout << "notas_alumnos = " << notas_alumnos << endl;
   int sumatorio = 0;
   float media = 0;
   notas_alumnos [ 2 ] += 3;// S� afecta fuera por ser tipo array, S� es un puntero en s�
   for ( int i = 0; i < TAM_ARRAY; i++ )
   {
      sumatorio += notas_alumnos [ i ];
   }
   media = ( float ) sumatorio / TAM_ARRAY;
   tamano = 200;  // NO afecta fuera por ser tipo primitivo, no un puntero
   return media;
}
#include "Header.h"

int global = 1;

// Funci�n que no son idempotentes
int f1 ( )
{
   global = 1;
   cout << "f1()\n";
   return 3 ;
}
int f2 ( )
{
   global = 0;
   cout << "f2()\n";
   return 4 ;
}
// Funci�n idempotentes: Cuando seg�n el par�metro siempre va a devolver el mismo resultado:
int f_idempot ( int x )
{
   return x * 3;
}
void ejemplo_operadores ( )
{
   int res = (f2 ( ) + f1 ( )) * global;
   cout << "res = " << res;
   cout << f_idempot ( 2 );
}
#include <iostream>

// #define MOSTRAR_TIT
// No es una variable, si no que se sustituye
#define CONST_PI     3.1415926
#define CONST_TIT    "Hello World!\n"
#define ELEVAR_CUBO(x)  ((x)*(x)*(x))

using namespace std;

int main ( )
{
#ifdef MOSTRAR_TIT
   Aquí pueden haber errores porque no se compila
      std::cout << CONST_TIT;
#endif

   cout << "Macro constante PI = " << CONST_PI << endl;

   cout << "Otra vez el titulo, 2 veces: " << CONST_TIT << ", " << CONST_TIT << endl;
   cout << "Otra vez el titulo, 2 veces: " << CONST_TIT << ", " << CONST_TIT << endl;
   cout << "10 al cubo: " << ELEVAR_CUBO ( 7 + 3 ) << endl;
   // Una macro es como sustituir el código directamente, por que lo hace preprocesador directametne el texto antes de compilar.
   cout << "10 al cubo: " << 10 * 10 * 10 << endl;
}

#include "Header.h"

void ejem_if_else ( )
{
   int x = 10;
   if ( x == 10 ) cout << "X es un 10" << endl;
   if ( x != 10 ) cout << "X no es un 10\n"; else cout << "X deber�a ser 10\n";

   if ( x == 7 )
   {
      cout << "Pues x es";
      cout << "7" << endl;
   } 
   else
   {
      cout << "Pues x no es 7" << endl;
   }
   int num_usu;
   cout << "Intro num: ";
   cin >> num_usu;
   cout << "Has puesto: ";
   if ( num_usu == 0 )
      cout << " cero" << endl;
   else if ( num_usu == 1 )
      cout << " uno" << endl;
   else if ( num_usu == 2 )
      cout << " dos" << endl;
   else if ( num_usu == 3 )
      cout << " tres" << endl;
   else
      cout << " muchos" << endl;
}
void ejem_switch ( )
{

   int x = 10;
   switch ( x ) {
      case 10:  cout << "X es un 10" << endl;
   }
   switch ( x )
   {
   case 10: cout << "X deber�a ser 10\n"; break;
   default: cout << "X no es un 10\n"; break;
   }
   int num_usu;
   cout << "Intro num: ";
   cin >> num_usu;
   cout << "Has puesto: ";
   switch ( num_usu )
   {
   case 0:
      cout << " cero" << endl; 
      break;
   case 1:
      cout << " uno" << endl;
      break;
   case 2:
      cout << " dos" << endl;
      break;
   case 3:
      cout << " tres" << endl;
      break;
   case 4:
   case 5:
   case 6:
      cout << "O bien 4, 5 o 6\n" << endl;
      break;
   default:
      cout << " muchos" << endl;
      break;
   }
}
#include "Header.h"

void ejem_for ( )
{
   // Uso t�pico:
         // Lleva una variable interna: sabemos donde empieza
         // Una condici�n de parada (sabemos donde acaba)
         // Una operaci�n sobre la variable interna
   // Pero en realidad un  bucle for es un bucle while con cierta estructura:
   // for ( <inicializaci�n> ;  <condici�n> ;  repetici�n )
      // inicializaci�n:   instrucci�n que se ejecuta una vez
      // condici�n:        es una expresi�n: el bucle se ejecuta mientras sea verdadera
      // repetici�n:       expresi�n que se ejecuta al terminar el bucle
   int num;
   cout << "Escribe un numero par: ";
   cin >> num;
   for ( ; num % 2 != 0; ) // Usandolo como un while
   {
      cin >> num;
   }
   for ( ; 4 == 3;  ) cout << "Nunca me ejecuto! ";
   for (cout << "Bucle infinito"; true; 5 * 3)  // USO EXTRA�O, PERO POSIBLE
   {
      cout << "Siempre me ejecuto! " << endl;
      // continue;   // Vuelve a la condici�n, pero no se recomienda su uso.
      cout << "Pon cero para salir:  ";
      cin >> num;
      if ( num == 0 ) break;  // Hay qui�n tampoco recomienda el break;
   }
   // Uso t�pico con una variable y un final claro
   cout << "Cuenta atras: " << endl;
   for ( num = 10; num > 0; num-- ) // No se ejectua si la condici�n es falsa (no muestra el cero)
   {
      cout << " - " << num << endl;
   }
   // Bucle for con dos variables:
   for ( double posA = 0 , posB = 300; 
      posA <= posB; 
      posA += 10.1 , posB -= 20.7 )
   {
      cout << " posA = " << posA << ", posB = " << posB << endl;
   }
   cout << "CHOCARON! " << endl;
}
void ejem_while ( )
{
   cout << "Escribe un numero par: ";
   {
      // El bucle se va a ejecutar mientras el usuario escriba impar,
      // no terminar� hasta que no escriba un par
      int num;
      cin >> num;
      // Primero se comprueba la condici�n, por lo tanto el bucle se puede repetir desde ninunga vez hasta el infinito y m�s all�.
      while (num % 2 != 0 ) cin >> num;
      cout << "Por fin has puesto un par! ";
      while ( 4 == 3 )
      {
         cout << "Nunca me ejecuto! ";
      }
      // Bucle infinito
      while ( true )
      {
         cout << "Siempre me ejecuto! " << endl;
         // continue;   // Vuelve a la condici�n, pero no se recomienda su uso.
         cout << "Pon cero para salir:  ";
         cin >> num;
         if ( num == 0 ) break;  // Hay qui�n tampoco recomienda el break;
      }
   }
}
void ejem_dowhile ( )
{
   int num;
   // El bucle do while, al menos se ejecuta vez�, porque la condici�n se compure al final.
   do
   {
      cout << "Escribe cero para salir ";
      cin >> num;
   } while ( num != 0 );
}
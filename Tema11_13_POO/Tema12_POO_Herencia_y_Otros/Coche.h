#pragma once

#include "Vehiculo.h"

class Coche : public Vehiculo
{
// private: No es obligatorio ponerlo, por defecto todo es privado
   /* string marca; Si dejamos las propiedades, ser�an nuevas
   string modelo; */
   // Variables est�ticas: s�lo existe una para todo el programa (para todas las clases)
   // En el fondo son como variables globales
   static int total_coches;

public:
   Coche ( string marca, string mode ) 
      : Vehiculo (marca, mode ) {  /* this->modelo = mode;*/
      cout << "Contruyendo coche " + this->marca + ", " << this->modelo << endl;
      this->total_coches ++;
   }
   static void mostrarTotalCoches ( );

   void apagar ( )
   {
      cout << "Apagando coche " << this->modelo << endl;
   }
   void acelerar ( )
   {
      cout << "Acelerando coche " << this->modelo << endl;
   }
   void setModelo ( string nuevoModel )
   {
      this->modelo = nuevoModel;
   }
   ~Coche ( )
   {
      Coche::total_coches--;
      cout << "ADIOS " << modelo << endl;
   }
};

#include "CocheElectrico.h"
#include "Moto.h"
#include "Fabrica.h"
#include <memory>

// Mejor uso de casting entre objetos y punteros:
// https://trucosinformaticos.wordpress.com/2017/11/11/cast-y-conversion-de-tipos-en-c/

/* Recibe una copia nueva de un coche, USA MÁS MEMORIA */
void usar_coche ( Coche c )  // Aquí de manera se crea la copia nueva
{
   cout << "->Usar copia de coche: " << endl;
   c.arrancar ( );

   // - Dentro de la función podríamos cambiar el objeto de manera TEMPORAL, la copia:
   c.setModelo ( "Otro modelo TEMP" );

   c.apagar ( );
}  // De manera automática se destruye la copia (parámetro, variable local)

/* Recibe una direcc mem de un coche:
*  Ventajas:     USA LA MEMORIA MÍNIMA
*/
void usar_coche_ptr ( Coche *ptr_c )   
{
   cout << "->Usar el mismo coche por PUNTERO: " << endl;
   ptr_c->arrancar ( );

   // - Dentro de la función podríamos cambiar el objeto de manera PERMAMENTE:
   ptr_c->setModelo ( "Otro modelo PERM" );

   ptr_c->apagar ( );
}

/* Recibe una direcc mem de un coche:
*  Ventajas:     USA LA MEMORIA MÍNIMA
*/
void usar_coche_ref ( Coche& ref_c )
{
   cout << "->Usar el mismo coche por REFERENCIA: " << endl;
   ref_c.arrancar ( );

   // - Dentro de la función podríamos cambiar el objeto de manera PERMAMENTE:
   ref_c.setModelo ( "Otro modelo PERM" );

   ref_c.apagar ( );
}
void coches ( )
{
   Coche* c1 = new Coche ( "Kia",  "Sportage" );
   Coche c2 = Coche ( "Seat", "Ibiza" );
   // usar_coche ( c2 );
    //usar_coche ( *c1 ); // indireción

    //usar_coche_ptr ( c1 );
   // usar_coche_ptr ( &c2 );  // dirección mem.
   //    usar_coche_ptr ( NULL ); Legal, sí se puede, pero casca
   //    usar_coche_ref ( NULL ); Ilegal, no se puede
   usar_coche_ref ( *c1 );  // Se pasa la referencia al contenido, por lo tanto se cambia permanentemente también
   usar_coche_ref ( c2 );  // dirección mem.
   // c2.mostrarTotalCoches ( ); Es correcto, pero no aduecuado
   Coche::mostrarTotalCoches ( );
    delete c1;
}
void otros_coches ( )
{
   Coche c3 ( "Mercedes", "Benz" );
   Coche *c4 = new Coche  ( "Renault", "Clio" );

   Coche::mostrarTotalCoches ( );

   delete c4;
}
void ejemplo_motos ( )
{
   Moto* moto1 = new Moto ( "Suzuki" , "Kawasaki" );
   Moto* moto2 = new Moto ( "Honda" , "CBR" );
   moto1->mostrar ( );
   moto2->mostrar ( );
   moto1->arrancar ( ); 

   cout << "Total motos " << Fabrica::getInstancia()->get_Motos ( ) << endl;
   delete moto1;
   delete moto2;
}
void ejemplo_fabrica ( )
{
   // Fabrica* f = new Fabrica(); No podemos porque es privado

   Coche* coche1 = Fabrica::getInstancia ( )->fabricarCoche ( "Aston Martin" , "Modelo AM" );
   Moto* moto2 = Fabrica::getInstancia ( )->fabricarMoto ( "Yamaha" , "Neo's" );
   coche1->arrancar ( );
   moto2->setModelo ( "Vespa" );
   moto2->mostrar ( );

   MotoElectrica* me = dynamic_cast<MotoElectrica *> (moto2);
   me->cargar_bateria ( 20 );

   delete coche1;
   delete moto2;
}
void ejemplo_vehiculus_electrico ( )
{
   CocheElectrico *ce = new CocheElectrico ("Tesla", "Model S", 206 );
   IVehiculoElectrico *ve1 = dynamic_cast< IVehiculoElectrico* > (ce);
   MotoElectrica* me2 = new MotoElectrica ( "Moto X" , "Electric Model" , 0 );     

   IVehiculoElectrico* ive2 = dynamic_cast < IVehiculoElectrico*> (me2); // new MotoElectrica ( "Moto X" , "Electric Model" , 0 ); 
   // Moto *mve2 = (Moto*) ve2;
   MotoElectrica* me2_bis = dynamic_cast < MotoElectrica*>( ive2);
   me2_bis->cargar_bateria ( 200 );

   IVehiculoElectrico* ve_el [ 2 ];
   ve_el [ 0 ] = dynamic_cast< IVehiculoElectrico* > (ve1);
   ve_el [ 1 ] = dynamic_cast< IVehiculoElectrico* > (ive2);
   for ( size_t i = 0; i < 2; i++ )
   {
      ve_el [ i ]->cargar_bateria ( 999 );
      cout << "Nivel de bateria vehiculo: " << ve_el [ i ]->get_nivel ( ) << endl;
   }
}
void ejemplo_veh ( )
{
   // No se puede instanciar un objeto de una clase abstracta
   /*Vehiculo* v = new Vehiculo ("No hay veh", "GGGG" );
   v->arrancar ( );*/
}
void main_polimorfismo ( );

int main()
{
  // ejemplo_veh ( );
   ejemplo_vehiculus_electrico ( );

   ejemplo_motos ( );
   ejemplo_fabrica ( );

   cout << "Total motos " << Fabrica::getInstancia ( )->get_Motos ( ) << endl;
   // coches ( );
   // otros_coches ( );
   return 0;
}

#pragma once

/* Una interfaz es una clase puramente abstracta con todos sus m�todos virtuales puros*/
class IVehiculoElectrico
{

public:
   /*virtual float get_nivel_bateria ( ) = 0;
   virtual void set_nivel_bateria ( float carga ) = 0;*/
   
   virtual void cargar_bateria ( float carga ) = 0;

   virtual float get_nivel ( ) = 0;
};
#pragma once

#include <iostream>

using namespace std;

/* Clase padre de Coche y Moto. Coche es un veh�culo y moto ES UN veh�culo
*/
class Vehiculo
{
protected:  // Es un nuevo nivel de acceso entre public y private:
   // Es p�blico para las clases que heredan de esta clase (Moto, Coche) pero privado para otras clases, como por ejemplo las que usan (Fabrica...)
   string marca;
   string modelo;

public:
   /*Vehiculo ( )
   {
      cout << "Contruyendo Vehiculo( ma, mo): " << marca << ", " << modelo << endl;
   }*/

   Vehiculo ( string marca , string mode )
      : modelo ( mode )
   {  /* this->modelo = mode;*/
      this->marca = marca;
      cout << "Contruyendo Vehiculo " + this->marca + ", " << this->modelo << endl;
      //apagar ( );
   }
   void arrancar ( )
   {
      cout << "Arrancando VEHICULO " << this->modelo << endl;
   }
   virtual void girarIzquierda ( )
   {
      cout << "Girar a la izquierda VEHICULO " << this->modelo << endl;
   }
   // La manera de hacer un m�todo abstracto es:
   virtual void apagar ( ) = 0;

   // M�todos abstractos (virtuales puros, m�todos de interfaz)
   virtual void acelerar ( ) = 0;

   void setModelo ( string nuevoModel )
   {
      this->modelo = nuevoModel;
   }
};


#include "CocheElectrico.h"
#include "Moto.h"
#include "Fabrica.h"

void ejemplo_polimorfismo ( )
{

   Moto* moto1 = new Moto ( "Suzuki" , "Kawasaki" );
   moto1->mostrar ( );
   moto1->arrancar ( );
   moto1->girarIzquierda ( );
   delete moto1;

   Vehiculo* moto2 = new Moto ( "Honda" , "CBR" );

   moto2->arrancar ( );
   moto2->acelerar ( );
   moto2->setModelo ( "Honda V" );
   moto2->girarIzquierda ( );

   cout << "Total motos " << Fabrica::getInstancia ( )->get_Motos ( ) << endl;
   delete moto2;
}
void ejem_array_clase_abstracta ( )
{
   Vehiculo* vehiculos [ 3 ];
   vehiculos [ 0 ] = new Coche ( "Nissan" , "Qasqui 4x4" );
   vehiculos [ 1 ] = new Moto ( "Honda" , "CBR" );
   vehiculos [ 2 ] = new CocheElectrico ( "Tesla" , "Model M", 200 );

   for ( int i = 0; i < 3; i++ )
   {
      vehiculos [ i ]->arrancar ( );
      vehiculos [ i ]->girarIzquierda ( );
      vehiculos [ i ]->apagar ( );
   }
}
void main_polimorfismo ( )
{
   // ejemplo_polimorfismo ( );
   ejem_array_clase_abstracta ( );
}
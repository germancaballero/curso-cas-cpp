#pragma once

#include "Vehiculo.h"
#include "IVehiculoElectrico.h"

class Moto : public  Vehiculo   /* La herencia se hace con : (dos puntos) y el nombre de la clase padre */
{
   /* Ya no tenemos que indicar las propiedades que ahora est�n en Veh�culo, PORQUE SE HEREDAN */

public:
   Moto ( string ma , string mo );
   void mostrar ( );

   void arrancar ( ) 
   {
      cout << "Arrancando MOTO " << this->modelo << endl;
   }
   void apagar ( ) override
   {
      cout << "Apagando MOTO " << this->modelo << endl;
   }
   void acelerar ( );

   void girarIzquierda ( ) override
   {
      cout << "Girar manillar a la izquierda " << this->modelo << endl;
   }
   ~Moto ( );

};

class MotoElectrica : public Moto , public IVehiculoElectrico
{

protected:
   double nivel_bateria;

public:
   MotoElectrica ( string ma , string mo, double nb ) : Moto ( ma , mo ), nivel_bateria(nb)
   {
   }
   void cargar_bateria ( float carga ) override
   {
      nivel_bateria += carga;
   }
   float get_nivel ( ) { return nivel_bateria; }
};


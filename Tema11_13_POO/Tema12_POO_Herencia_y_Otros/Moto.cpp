#include "Moto.h"
#include "Fabrica.h"

Moto::Moto ( string ma , string mo )
// Para llamar al consturctor del padre:
   :Vehiculo(ma, mo )
{
   cout << "Contruyendo moto( ma, mo): " << ma << ", " << mo << endl;
   /* marca = ma;  Ya no hace falta porque se hace en el padre
   modelo = mo; */
   Fabrica::getInstancia ( )->addMotos();
}

void Moto::mostrar ( )
{
   cout << "Moto: " << marca << ", modelo: " << modelo << endl;
}
void Moto::acelerar ( )
{
   cout << "Acelerando moto " << this->modelo << endl;
}
Moto::~Moto ( )
{
   Fabrica::getInstancia ( )->removeMotos ( );
}
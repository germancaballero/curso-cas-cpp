#pragma once

#include "Coche.h"
#include "Moto.h"

class Fabrica
{
private:
   int total_motos;
   static Fabrica *instancia;
   
   Fabrica ( )
   {
      this->total_motos = 0;
   }
   
public:
   static Fabrica*& getInstancia ( )
   {
      if ( instancia == NULL )
      {
         // Esta sentencia s�lo se ejectur� una vez en todo el programa
         instancia = new Fabrica ( );
      }
      return instancia;
   }

   Coche* fabricarCoche ( string marc , string mode );
   Moto* fabricarMoto ( string marc , string mode );

   void addMotos ( );
   void removeMotos ( );
   int get_Motos ( );
};


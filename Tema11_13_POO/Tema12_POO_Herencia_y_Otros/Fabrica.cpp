#include "Fabrica.h"

Fabrica* Fabrica::instancia = NULL;

Coche* Fabrica::fabricarCoche ( string marc , string mode )
{
   Coche* nuevoCoche;
   nuevoCoche = new Coche ( marc , mode );
   return nuevoCoche;
}
Moto*  Fabrica::fabricarMoto ( string marc , string mode )
{
   return new Moto ( marc , mode );
}
int Fabrica::get_Motos ( )
{
   return Fabrica::total_motos;
}
void Fabrica::addMotos ( )
{
   Fabrica::total_motos ++ ;
}
void Fabrica::removeMotos ( )
{
   Fabrica::total_motos --;
}
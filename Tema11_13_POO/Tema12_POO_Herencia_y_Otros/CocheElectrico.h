#pragma once

#include "Coche.h"
#include "IVehiculoElectrico.h"

class CocheElectrico : public Coche, public IVehiculoElectrico
{
private:
   float nivel_bateria;

public:

   CocheElectrico ( string m , string ma , float bat ) : Coche ( m , ma ) , nivel_bateria ( bat )
   {
      cout << "Construyendo CocheElectrico: " << m << ", " << ma << endl;
   }

   void cargar_bateria ( float carga ) override
   {
      nivel_bateria += carga;
   }
   float get_nivel ( ) { return nivel_bateria; }
};


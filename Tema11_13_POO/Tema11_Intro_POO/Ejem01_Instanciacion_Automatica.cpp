#include "Header.h"

void fun_crear_usu ( )
{
   fun_par ( 20 );
   std::cout << "Programacion Orientada a Objetos:\n";
   Usuario usu ( "Zulano" , 35 , 1.66 );
   // usu.nombre = "Fulanito";
   // usu.edad = 28;

   usu.mostrar ( );
   fun_usu_def ( );
   Usuario usu2;
   // Usuario usu2 ;
   usu2.mostrar ( );
   usu2.setNombre ( "Mengano" );
   usu2.mostrar ( "USU2 = " );
   int num ( 3 );   // eqv -> int num = 3 
   cout << "num = " << num << endl;
}
// Ejercicio: 
//  -  getter y setter de edad, de altura, 
//  -  un constructor que reciba s�lo nombre y edad, (altura a cero)
//  -  M�todo que devuelva true/false si es mayor de edad
//  -  M�todo que pida al usuario que introduzca su  edad y altura (cin)

void fun_par ( int num )
{
   for ( int i = 0; i <= num; i += 2 )
      cout << ", " << i;
   cout << endl;
}

void fun_usu_def ( )
{
   UsuarioDef udf ( 1.77 , "Juan " );
   udf.mostrar ( "fun_usu_def -> UDF = " );
   udf.mostrar ( );
}
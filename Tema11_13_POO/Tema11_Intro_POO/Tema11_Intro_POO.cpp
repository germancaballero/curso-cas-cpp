#include "Header.h"

/*
   Programación Orientada a Objetos:
      1 - Es un patrón de diseño, donde tenemos objetos que se basan en clases (estructuras con métodos). Lo que ocurre es que es tan diferenciador que se considera un paradigma, una forma diferente de trabajar de la prog. estructurada/funcional.
      2 - Encapsulación: La capacidad de publicar/hacer privadas propiedades y/o métedos
      3 - Herencia: La capacidad de que una clase herede prop y métodos de otra
      4 - Polimorfismo: La capacidad de que un objeto tenga la forma suya, o de alguno de sus ascendientes (padre, abuelo...). Pero una instancia de un objeto padre NO PUEDE tener la forma de uno de sus hijos.
*/

int main ( )
{
    fun_crear_usu ( );
   // fun_crear_new_usu ( );
}
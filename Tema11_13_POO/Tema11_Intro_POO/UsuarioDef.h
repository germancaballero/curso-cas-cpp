#pragma once
// #include "Header.h"

#include <iostream>
#include <string>
#include "Usuario.h"

using namespace std;

class UsuarioDef
{
public:
   // Antes de llamar a cualquiera de los constructores, se instancian los objetos declarados en sus variables miembro (se instancian las propiedades)
   UsuarioDef ( string nombre = "Sin nombre" , int edad = 0, float altura = 0 )
      : nombre ( nombre ) , edad ( edad ) , altura ( altura ) 
   {
   }

   UsuarioDef (  float altura , string nombre )
      : nombre ( nombre ) , edad ( 0 ) , altura ( altura )
   {
      cout << "Contruyendo UsuarioDef -> " << nombre << endl;
      string nombre_sec = string ( "SEC - " + nombre );
      // Aqu� se duplica la estructura (objeto)
       this->usu_secundario = Usuario ( nombre_sec , edad );
      /*this->usu_secundario.setNombre ( nombre_sec );
      this->usu_secundario.setEdad ( edad );*/
   }
   void mostrar (string mensaje = "" )
   {
      cout << mensaje << "DEF-> Nombre : " << this->nombre
         << ", edad: " << ( unsigned short ) this->edad << ", alt: " << altura << endl;
   }

   // Variable est�tica: Existe una �nica instancia/valor de esta variable
   static short totalUsuarios;

   ~UsuarioDef ( )
   {
      // Se va a eliminar el objeto. Aqu� eliminar�amos otros objetos relevante (p.ej. sus propiedades)
      cout << "~UsuarioDef(): " << nombre << endl;
   }
private:
   string nombre;
   unsigned char edad;
   float altura;
   Usuario usu_secundario;
};


#include "Usuario.h"

// NombreClase::Metodo (par�metros) { /*cuerpo fun */ }
Usuario::Usuario ( )
{
   cout << "Contruyendo Usuario " << nombre << endl;
   this->nombre = "Sin nombre";
   this->edad = 0;
   this->altura = 0;
   this->ptr_direccion = new Direccion ( "Avenida de " + nombre , 11 );
}
Usuario::Usuario ( string nomb , int eda , float altura )
{
   cout << "Contruyendo Usuario " << nombre << endl;
   // La palabra clave this es un par�metro que siempre disponemos con un puntero al objeto asociado a este m�todo. De hecho justo antes de llamarse al constructor es donde se instancia (reserva la memoria para "this")
   this->nombre = nomb;
   edad = eda;
   // Aqu� estamos obligados para evitar confusiones
   this->altura = altura;
   this->ptr_direccion = NULL;
}

void Usuario::mostrar ( string mensaje )
{
   cout << mensaje <<  "Nombre : " << this->nombre 
        <<  ", edad: " << (unsigned short) this->edad << endl;
}
void Usuario::setNombre ( string nombre )
{
   this->nombre = nombre;
}
string Usuario::getNombre (  )
{
   return this->nombre;
}

void Usuario::setEdad ( unsigned char edad )
{
   this->edad = edad;
}
unsigned char Usuario::getEdad ( )
{
   return this->edad;
}

Usuario::~Usuario ( )
{
   // Se v  a a eliminar el objeto. Aqu� eliminar�amos otros objetos relevante (p.ej. sus propiedades)
   cout << "~Usuario(): " << nombre << endl;
   if (ptr_direccion != NULL )
      delete ptr_direccion;
}
#include "Header.h"

void fun_crear_new_usu ( )
{
   int x = 10;    // No podemos cambiar su direcci�n de memoria porque es gestionada por el compilador. S�lo podemos saber cual es con &x
   cout << "Creando new usu: " <<  sizeof(Usuario) << endl;
   /*
   Usuario* ptr_usu_1 = ( Usuario* ) malloc ( sizeof ( Usuario ) ); */
   // La palabra new es similar a malloc, pero reserva bien las variables miembro e invoca al constructor.
   Usuario* ptr_usu_1 = new Usuario ( );
   ptr_usu_1->setAltura ( 1.79 );
   ptr_usu_1->setEdad ( 45 );
   ptr_usu_1->setNombre ( "Lorenzo" );

   ptr_usu_1->mostrar ( );
    // free(ptr_usu_1) y llamar destructor
   delete ptr_usu_1;

   Usuario* ptr_usu_2 = new Usuario ("Ana", 35 );
   ptr_usu_2->mostrar ( );
   delete ptr_usu_2;
} 
// str1 = "kjljlk" + "sdffdg"
// str2 = "jkljlkj" + str1;
/*
struct Usuario {
   // Todo arrays:
   char* nombre;  // malloc
   char* direccion;   // malloc
   int* notas;   // malloc
}
u = (Usuario*) malloc (sizeof(Usuario));
u->nombre = malloc (50);
// Todos los free: primero propiedades y �ltimo el usuario
*/
#pragma once

//  #include "Header.h"
#include <iostream>
#include <string>
// #include "UsuarioDef.h"
// class Usuario;

using namespace std;

class Direccion
{
public:
   Direccion ( string calle , short cp ) : calle ( calle ) , cp ( cp ) {
      cout << "Contruyendo direcc: " << calle << endl;
   }
   string calle;
   short cp;
   ~Direccion ( )
   {
      cout << "Destruyendo direcc: " << calle << endl;
   }
};

class Usuario
{
public:// Modificador de acceso para hacer propiedades  p�blicas (accesibles desde fuera)
   /* Constructores: Es el m�todo que se llama justo despu�s de instanciarse (reserva de la memoria en el objeto this). */
   Usuario ( );
   // Los contructores tambi�n  pueden ser inline
   Usuario ( string nombre , int edad )
      : nombre(nombre ) , edad(edad ), altura(0 )
   {
      cout << "Contruyendo Usuario " << nombre << endl;
      this->ptr_direccion = new Direccion ( "Calle de " + nombre , 99 );
/* Esto ya lo hacemos en la declaraci�n       this->nombre = nombre;
      this->edad = edad;
      this->altura = 0; /**/
   }
   // El constructor NO devuelve un tipo, por que en s� mismo es un tipo
   Usuario (string nom, int ed, float altura );
   
   void mostrar ( string mensaje = "" );
   // M�todos de acceso a variables miembro:
   void setNombre ( string nombre );
   string getNombre ( );
   void setEdad ( unsigned char edad );
   unsigned char getEdad ( );
   inline void setAltura ( float altura )
   {
      this->altura = altura;
   }
   /*inline es lo mismo */ float getAltura ( )
   {
      return this->altura;
   }


   /* Destructores: Es el m�todo que se llama justo antes de eliminarse (liberarse la memoria) el objeto (this). */
   ~Usuario ( );
private: // Modificador de acceso para hacer propiedades no p�blicas (privadas)
   // Las variables miembro siempre deben ser privadas por precauci�n.
   
   string nombre;
   unsigned char edad;
   float altura;
   Direccion* ptr_direccion = NULL;
};


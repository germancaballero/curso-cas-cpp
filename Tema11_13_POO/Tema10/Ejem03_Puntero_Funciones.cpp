// https://robologs.net/2019/05/31/tutorial-punteros-a-funciones-en-c/

#include <iostream>

using namespace std;
#include <stdio.h>

int sumar ( int a , int b )
{
   return a + b;
}

int restar ( int a , int b )
{
   return a - b;
}
int multiplicar ( int a , int b )
{
   return a * b;
}
// El puntero a funci�n es lo que se denomina funci�n  callback (o funci�n de retro llamada)
// El uso es que parte de la funcionalidad de esta funci�n est� externalizada, 
void funcion_principal ( int a , int b , int ( *funcion )( int , int ) )
{
   int resultado = funcion ( a , b );
   printf ( "El resultado es %d\n" , resultado );
}

int ej_puntero_funcion ( )
{
   //Se definen dos valores enteros cualesquiera
   int num1 = 5;
   int num2 = 4;

   //Se invoca la funcion principal, pasandole la funcion de SUMA
   printf ( "\nSuma:\n" );
   funcion_principal ( num1 , num2 , sumar );

   //Se invoca la funcion principal, pasandole la funcion de RESTA
   printf ( "Resta:\n" );
   funcion_principal ( num1 , num2 , restar );
   printf ( "Mult:\n" );
   funcion_principal ( num1 , num2 , multiplicar );

   /*funcion_principal ( num1 , num2 , [ & ] ( int a, int b ) -> int
      {
         return a + a;
      } );*/

   return 0;
}
// https://tutorialesprogramacionya.com/cmasmasya/detalleconcepto.php?punto=50&codigo=181&inicio=45

#include<iostream>

using namespace std;
class Punto
{
public: 
   int x , y;
};
class Temperatura
{
private:
   int minima , maxima , actual;
public:
   Temperatura ( int min , int max , int act ) { 
      minima = min; maxima = max; actual = act; 
   }
    friend int temperaturaMedia ( Temperatura t1 , Temperatura t2 );
   static int st_temperatura_media ( Temperatura t1 , Temperatura t2 );

   /*int get_min ( ) { return minima; }
   int get_max ( ) { return maxima; }
   int get_actual ( ) { return actual; }*/
};

int Temperatura:: st_temperatura_media ( Temperatura t1 , Temperatura t2 )
{
   int pro = ( t1.actual + t2.actual ) / 2;
   return pro;
}
int temperaturaMedia ( Temperatura t1 , Temperatura t2 )
{
   int pro = ( t1.actual + t2.actual ) / 2;
   return pro;
}

int ejem_clases_amigas ( )
{
   Temperatura temperatura1 ( 10 , 20 , 15 );
   Temperatura temperatura2 ( 12 , 25 , 17 );
   cout << "La temperatura promedio de las temperaturas actuales es:";
   cout << temperaturaMedia ( temperatura1 , temperatura2 );
   return 0;
}
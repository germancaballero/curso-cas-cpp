#include <iostream>

int ejem_recursividad ( );
int ejem_lambdas ( );
void ejemplo_const_param ( );
int ej_puntero_funcion ( );
void ej_auto ( );
int ejem_clases_amigas ( );

int main()
{
   //ej_auto ( );
   // ej_puntero_funcion ( );
   // ejemplo_const_param ( );
   // ejem_recursividad ( );
   // ejem_clases_amigas ( );
   ejem_lambdas ( );
}

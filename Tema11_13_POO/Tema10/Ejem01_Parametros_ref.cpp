// https://tutorialesprogramacionya.com/cmasmasya/detalleconcepto.php?punto=60&codigo=191&inicio=45

#include <iostream>

using namespace std;

class Coche
{
public:
   string marca;
   string modelo;
};

void mostrar_coche (const Coche& coche)
{
   // coche.marca = "kjljk";  No podemos porque es const
   cout << "Marca: " << coche.marca << ", modelo: " << coche.modelo << endl;
}

void ejemplo_const_param ( )
{
   Coche c;
   c.marca = "Ford";
   c.modelo = "Mondeo";
   mostrar_coche ( c );
}
#include <iostream>
#include <functional>

using namespace std;

// https://openwebinars.net/blog/que-son-las-funciones-lambdas-en-cpp/

/*int f ( int a )
{
   return a + a;
};*/
int recibe_callback (int param, std::function<int(int&)> lambda )
{
   cout << "Recibe callback: resultado = " << lambda ( param ) << endl;
   return lambda ( param );
}

int ejem_lambdas ( )
{
   // Una funci�n lambda es una funci�n que no tiene nombre y que es asignable a una variable o f�cilmente pasable como argumento a una funci�n, su uso suele ser como funci�n callback
   auto f = [ & ] ( int& a ) -> int
   {
      return a + a;
   };
   int a = 10;
   std::cout << f ( a ) << std::endl;
   std::cout << recibe_callback ( a, f ) << std::endl;

   std::cout <<
      recibe_callback (
         a ,
         [ & ] ( int& l ) -> int { return l * l; } // Creamos una funci�n, la pasamos como par�metro, y despu�s se desecha
      ) << std::endl;

   recibe_callback (
      60 ,
      [ & ] ( int& x ) -> int { return  x * x * x; } // Creamos una funci�n, la pasamos como par�metro, y despu�s se desecha
   );
   return 0;
}
/*
int ejem_lambdas ( )
{
   int b = 10;
   int c = 100;
   auto f = [ & ] ( int& a ) -> int
   {
      b += 1;
      c += 1;
      return a + b + c;
   };
   int a = 10;
   std::cout << f ( a ) << std::endl;
   std::cout << b << std::endl;
   std::cout << c << std::endl;
   return 0;
}*/
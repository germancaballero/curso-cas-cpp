﻿#include "Header.h"

void main_enteros ( void )
{
   // bool se almacena en 1 byte tambien
   bool verdadero = true;
   // char es un número entero que se almacena 1 byte: -128 a 127
   char num = 60 + 6;
   // char es un número entero que se almacena 2 bytes: -32768 a 32767
   short peq = 30000;
   // int es un número entero que se almacena 4 bytes: -2147483648 a 2147483647
   int gran = 2100000000;
   // long es un número entero que se almacena 4 bytes: -2147483648 a 2147483647
   long gran2 = 2100000000;
   // long es un número entero que se almacena 8 bytes: -2147483648 a 2147483647
   long long muygran = 2500000000;
   gran = (int) muygran;   // Casting explícito
   // size_t es el tipo de variable para almacenar tamaños de variables u otros tipos de datos
   size_t tam_int = sizeof ( int );
   cout << "Un int ocupa " << tam_int << endl;
   long long un_array_longs [ 300 ];
   size_t tam_array = sizeof ( un_array_longs );

   cout << "tam_array ocupa " << tam_array << endl;
   // Entero para guardar el tamaño de una variable:
   // En 32 bits, el tamaño de una variable que guarde un tamaño son 4 bytes, per van sin signo
   //             por lo que el limite es de 0 a 4200000000
   // En 64 bits, son 8 bytes, 0 hasta   18446744073709551616
   size_t tam = sizeof ( size_t );

   // size_t es unsinged. unsigened lo podemos combinar con cualquier tipo de entero
   unsigned char uc = 100;   // 0  a 255

   std::cout
      << "bool = " << verdadero << "\n"
      << "char = " << num << ", " << ( short ) num << "\n"
      << "short = " << peq << "\n"
      << "int = " << gran << "\n"
      << "long = " << gran2 << "\n"
      << "long long = " << muygran << "\n"
      << "size_t = " << tam << "\n"
      << "uc = " << uc << ", " << ( short ) uc << "\n";


}

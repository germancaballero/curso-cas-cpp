#include "Header.h"
#include <iomanip> 

void main_flotantes ( void )
{
   float pi_f = 3.14159253;
   cout << "pi_f = " << pi_f << endl;
   double pi_d = 3.14159253;
   cout << "pi_d = " << pi_d << endl;

   /*
   * Valor exacto:
   * -6784324679832    ->    -6.784324679832 x 10 ^ 12
   * valor almacenado:        6.784324000 * 10 ^12
   */
   float f = 6784324679832;   
   cout << "f = " << f << endl;  

   cout << "sizeof   f = " << sizeof(f) << endl; // 4 bytes
   // De los 4 bytes, 3 son para la mantisa, y 1 para el exponente
   // Se almacena como mantisa * 2 ^ exponente
   // P.ej la mantisa podr�a ser: 1 001 0101  0101 1101 0011 1111
   //       En la mantisa tenemos un bit para +/- y los otros 23 bits para las cifras
   //       Que en valores decimales, sean unas 7 � 8 cifras decimales
   // P. ej exponente podr�a: 0 100 0110
   //       1 bit para +/-  y 7 bits para el exponente (es decir, es como un char con signo)
   //       Es decir, el l�mite del exponente es de -128 a 127
   //       Es decir tambi�n, el l�mite del exponente es
   //          -3,40282 * 10 ^ -38    hasta +1,7014118 * 10 ^ 38   

   // En el caso del double: Se usan 8 bytes: Repartidos:
   //  11 + 1 bits para el exponente   - 2 ^ 2048 hasta + 2 ^ 2048 - 1
   //  51 + 1 bits para la mantisa  // Como m�ximo son unas 15 � 16 cifras decimales
   double num_tocho = 23453254325443232000000004354354354235.43542;
   cout << "num_tocho = "  << num_tocho << endl;

   double double_f = f; // Casting impl�cito.
   float float_d = num_tocho;
   cout << "float_d = " << float_d << endl;
   cout << "double_f = " << double_f << endl;

   long double super_pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;
   cout << "tam long double = " << sizeof (super_pi) << endl;
   std::cout << std::fixed             // fix the number of decimal digits
         << std::setprecision ( 18 );  // to 18
   cout << "super_pi = " <<  super_pi  << endl;
   printf ( "printf >>> super_pi = %1.18lf \n" , super_pi );

}
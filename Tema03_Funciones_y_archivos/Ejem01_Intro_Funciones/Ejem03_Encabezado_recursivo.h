#pragma once

#include <iostream>
#include "Ejem03_Header.h"

void funcion_implementada_en_h ( );
// Sobrecargando con diferente firma (nombre + param)
void funcion_implementada_en_h ( int x );

/*{
   std::cout << "funcion_implementada_en_h() \n";
}*/
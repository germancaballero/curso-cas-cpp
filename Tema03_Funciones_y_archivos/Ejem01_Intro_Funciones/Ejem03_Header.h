// #pragma once 

// EL PREPROCESADOR TRATA S�LO TEXTO ANTES DEL COMPILADOR

// #if es una directiva del preprocesador que permite incluir o no un trozo de c�digo seg�n una condici�n del preprocesador (no se pueden usar variables de C++

// #ifndef es una directiva del preprocesador que permite incluir o no un trozo de c�digo seg�n exista o no la definici�n
#ifndef _EJEM03_HEADER_H_

// Definimos una definici�n (una palabra del preprocesador)
#define _EJEM03_HEADER_H_

#include <iostream>
#include "Ejem03_Encabezado_recursivo.h"

void funcion_declarada_en_h ( );

#endif    // _EJEM03_HEADER_H_
#include "Ejem03_Header.h"

void funcion_declarada_en_h ( )
{
   std::cout << "funcion_declarada_en_h ( )\n";
   std::cout << "Llamamos a otro fichero\n";
   // Las funciones sobrecargadas, se pueden usar y el compilador elige cual llamar por los tipos de los valores de los parámetros
   funcion_implementada_en_h ( );
   funcion_implementada_en_h (6 );
}
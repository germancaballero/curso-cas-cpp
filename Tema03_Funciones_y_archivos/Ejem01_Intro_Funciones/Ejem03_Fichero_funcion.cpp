#include "Ejem03_Encabezado_recursivo.h"

void funcion_implementada_en_h ( )
{
   std::cout << "funcion_implementada_en_h() \n";
}
// Sobrecargando con diferente firma (nombre + param)
void funcion_implementada_en_h (  int x)
{
   std::cout << "funcion_implementada_en_h() x = " << x << " \n";
}

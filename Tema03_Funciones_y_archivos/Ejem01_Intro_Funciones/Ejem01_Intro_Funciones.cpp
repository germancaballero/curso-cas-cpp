// Ejem01_Intro_Funciones.cpp : 

 #include <iostream>
// Como el include es un copia y pega, "pegamos" aquí las declaraciones
#include "Ejem03_Header.h"

// Función que sólo está implementada
void funcion_sin_declarar ( )
{
   std::cout << "funcion_sin_declarar ( ): directamente programada" << std::endl;
}
/* DECLARACIONES DE FUNCIONES */
// Para declarar una función, se pone "la firma" de la función sin el cuerpo y ;
// La firma de las funciones incluyen: 
// Primero el tipo que devuelve, el nombre, y los parámetros que recibe
void funcion_declarada ( );
// Función con parámetros, sin valor devuelto
void funcion_con_param ( int p1 , float p2 );
// Función valor devuelto sin párametros:
int funcion_que_pide_valor ( );
// Función con parámetros y que devuelve un valor:
// En la declaración, no importa el nombre de los parámetros. Sólo importa el tipo
int devuelve_suma ( int , int no_importa);
void funcion_externa ( int );
void funcion_externa2 ( int );
void otra_funcion ( );

// Podemos declarar todas las veces que queramos, pero mejor usar el encabezado
/*void funcion_declarada_en_h ( );
void funcion_declarada_en_h ( );
*/

int main()
{
   /* LLAMADAS A FUNCIONES */
   std::cout << "Tipos de funciones:\n";
    funcion_sin_declarar ( );
    funcion_declarada ( );
    // Los argumentos serán los futuros parámetros
    funcion_con_param ( 22 , 3.14159253 );

    // El valor que devuelve una función, lo podemos almacenar en otra variable...
    int val_usu = funcion_que_pide_valor ( );
    std::cout << "Has escrito " << val_usu << std::endl;
    // ... o usar directamente
    std::cout << "La suma 3 + 7 es " << devuelve_suma ( 3 , 7 ) << std::endl;
    funcion_externa ( 7 );
    funcion_externa2 ( 7 );
    otra_funcion ( );
    funcion_declarada_en_h ( );
}
/* IMPLEMENTACIONES DE FUNCIONES  */
void funcion_declarada ( )
{
   std::cout << "funcion_declarada ( );" << std::endl;
}
// Parámetros tendrán los valores de los argumentos
void funcion_con_param ( int p1 , float p2 )
{
   std::cout << "funcion_con_param: " << p1 << ", " << p2 << std::endl;
}

int funcion_que_pide_valor ( )
{
   int num;
   std::cout << "Escribe un número: \n";
   std::cin >> num;
   // Para devolver un valor, usamos return
   return num;
}

// En la implementación, sí importa el nombre de los parámetros por que son variables locales
int devuelve_suma ( int x, int y )
{
   return x + y;
}
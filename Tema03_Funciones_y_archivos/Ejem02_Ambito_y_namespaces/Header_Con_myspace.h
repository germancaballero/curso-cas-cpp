#pragma once

#include <iostream>

// Indicamos que queremos usar todo el espacio de nombres para evitar el prefijo    std::
using namespace std;

namespace myspace
{

   typedef int entero;

   void funcion_cualquiera ( void );
   int ternario ( int a , int b , int verdadero , int falso );
   int ternario_bis ( int a , int b , int verdadero , int falso );
}
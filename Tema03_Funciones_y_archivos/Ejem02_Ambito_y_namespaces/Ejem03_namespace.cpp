
#include "Header_Con_myspace.h"

// Un espacio de nombres es un conjunto de cualquier cosa nombrable en C y C++:
// variables globales, funciones, tipos (tipos renombrados, estructuras, clases)

// Para crear uno nuevo, usamos la palabra clave namespace con un bloque que contenga parte de los nombres, ya que en otro fichero podemos ir a�adiendo nuevos  nombres

namespace myspace
{
   void funcion_cualquiera (void )
   {
      cout << "cout sin std::\n";
      cout << "Escribe un num: \n";
      entero x = 10;
      cin >> x;
      cout << "x = " << x << endl;
      return;
   }
}

#include "Header_Con_myspace.h"


namespace myspace
{
   int ternario ( int a , int b , int verdadero , int falso )
   {       
      char condicion = ( a < b ) * 10 ;
      return condicion ? verdadero : falso;
   }
   int ternario_bis ( int a , int b , int verdadero , int falso )
   {
      bool condicion = ( a < b );
      return condicion * verdadero + (1 - condicion) * falso;
   }
}
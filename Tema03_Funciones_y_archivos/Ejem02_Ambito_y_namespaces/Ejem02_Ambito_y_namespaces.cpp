#include <iostream>
#include "Header_Con_myspace.h"

using namespace myspace;

void otra_func ( void )
{
   std::cout << " otra_func\n";
}

int funcion_con_var ( int x )   // Los param son variables locales
{
   int v_local = 4;
   std::cout << " v_local = " << v_local;
   // Aunque aquí modifiquemos una variable, fuera "no se modifica"
   x = x * 2;
   std::cout << " x = " << x << std::endl;
   otra_func ( );
   return v_local + x;
}  // Al terminar la función se libera la memoria de toooodas las variables locales


int main (void )
{
   int v = ternario ( 35 , 55 , 7 , 2 );
   int v2 = ternario_bis ( 35 , 55 , 7 , 2 );
   cout << "Resultado ternario: " << v << endl;
   cout << "Resultado ternario: " << v2 << endl;
   funcion_cualquiera ( );

   std::cout << "Ambito variables:\n";
   // Las laves declaran un bloque: que es un conjunto de instrucciones que se pueden tratar como una sóla instrucción. El ambito de las variables declaradas dentro es local al bloque.
   {
      std::cout << "Bloque 1:\n";
      {
         // Es el compilador el que se encarga de reservar los 4 bytes para la var
         int var_local = 10;
         std::cout << "Bloque 1.1: " << var_local << " \n";
      }
      // Aquí deja de existir la variable, se libera esa memoria y ya no se puede usar
      //std::cout << "No va: " << var_local << " \n";
      {
         std::cout << "Bloque 1.2:\n";
      }
      funcion_con_var ( 8 );
      int futura_x = 30;
      int ret = funcion_con_var ( futura_x );
      std::cout << "futura_x = " << futura_x << "\n";
      std::cout << "ret = " << ret << "\n";
      funcion_con_var ( 100 );

      // Aquí x no existe
   }
}

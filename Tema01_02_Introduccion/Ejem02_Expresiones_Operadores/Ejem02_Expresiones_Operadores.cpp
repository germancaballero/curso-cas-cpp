#include <iostream>



int main()
{
   /// std::cout << x * y + 30 / 10;  NO SE PUEDE USAR ANTES DE DECLARAR
   int x = 10 , y = 20;
   /* Este operador tiene más prioridad que nadie, el de Resolución de ámbito    :: */
    std::cout << "Expresiones y operadores!\n";

    // y++   es lo mismo que y = y + 1
    // El operador de postfijo ++ (ó el --) se ejecuta después de la instrucción
    // ¿ (20 + 1) * 2 = 42   ó  20 * 2  + 1 = 41?
    y = y++ * 2;  // 20 * 2  + 1 = 41

    // ¿ (10 + 1) * 2 = 22   ó  10 * 2  + 1 = 11?
    x = ++x * -2;  // (10 + 1) * 2 = 22
    std::cout << "x = " << x << ", y = " << y << std::endl;
    std::cout << x * (y + 30) / 10 << std::endl;
    std::cout << "Cada int ocupa " << sizeof ( x ) << " bytes" << std::endl;
    int num = 44;    // 0010 1100
    num = num >> 2;   // 0000 1011   = 11
    int n2 = 19;      // 0001 0011 
    std::cout << "num = " << num << std::endl;
    std::cout << " AND = " << (num & n2) << std::endl; // 3
    std::cout << "  OR = " << ( num | n2 ) << std::endl; // 27
    std::cout << " XOR = " << ( num ^ n2 ) << std::endl; // 24

    int j = 10;
    j += 5; // lo mismo que j = j + 5
    std::cout << "j = " << j << std::endl;

    // Un único operador ternario: recibe 3 valores y devuelve 1:

    int t = ( 4 < 3 ) ? 2 : 3;
    std::cout << "t = " << t << std::endl;

}


// Ejem01_Intro.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.

/*
* Explicación del fichero.
* Comentario de varias lineas
*/

// Directivas del preprocesador. Empiezan coin #
#include <iostream> 
// Preprocesador: Hace tareas a nivel de código fuente antes del compilador:
// #include Copia y pega un fichero donde se usa
// iostream = Flujo de E/S

int main()  // main es una función principal que se ejecuta al iniciar el programa
{
	// std = biblioteca standar
	// cout = console out
	std::cout << "Hola mundo!" << std::endl << "Otra linea " << 2 << " lineas";
	std::cout << "Que hay" << std::endl;
	// Declaración variable: tipo nombre_variable, var_2, var_3;
	int numero_2,
		n2 = 20,
		n3;
	// Asignación:
	numero_2
		=
		10;
	int double_3 = 10;
	std::cout << "\n numero = " << numero_2 << std::endl
		<<  // Una línea de código puede ocupar varias
		"Introduzca un numero: ";
	std::cin >> n3;
	std::cout << "Tu num: " << n3;
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
